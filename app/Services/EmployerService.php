<?php

namespace App\Services;

use App\Dto\EmployerDto;
use App\Models\Employer;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

final class EmployerService
{
    private const LIMIT_DEFAULT = 10;

    public function getAll(array $request): LengthAwarePaginator
    {
        return Employer::query()
            ->paginate($request['per_page'] ?? self::LIMIT_DEFAULT);
    }

    public function create(EmployerDto $employerDto): Employer
    {
        $employer = new Employer();

        $this->setData($employerDto, $employer);

        $employer->save();

        return $employer;
    }

    public function update(EmployerDto $employerDto, Employer $employer): Employer
    {
        $this->setData($employerDto, $employer);

        $employer->save();

        return $employer;
    }

    public function delete(Employer $employer): bool
    {
        if ($employer->delete()) {
            return true;
        }

        return false;
    }

    public function search(string $searchData): Collection
    {
        return Employer::query()
            ->where('fname', 'like', "%{$searchData}")
            ->orWhere('sname', 'like', "%{$searchData}")
            ->orWhere('pname', 'like', "%{$searchData}")
            ->get();
    }

    private function setData(EmployerDto $employerDto, Employer $employer): void
    {
        $employer->sname = $employerDto->getSname();
        $employer->fname = $employerDto->getFname();
        $employer->pname = $employerDto->getPname();
    }
}
