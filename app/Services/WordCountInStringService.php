<?php

namespace App\Services;

use Illuminate\Support\Str;

final class WordCountInStringService
{
    public const WORDS = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
     been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and
     scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into
     electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of
      Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus
      PageMaker including versions of Lorem Ipsum.';


    public function countWord(): array
    {
        $str = Str::lower(self::WORDS);

        $wordArray = str_word_count($str, 1);
        $diffWord = array_diff($wordArray, []);
        $coincidenceWordArray = array_count_values($diffWord);
        arsort($coincidenceWordArray);

        $result = (array_slice($coincidenceWordArray, 0, 1));

        return ['word_count' => array_values($result)[0]];
    }
}
