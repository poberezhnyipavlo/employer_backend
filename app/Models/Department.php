<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 * @property string name
 *
 * Class Department
 * @package App\Models
 */
class Department extends Model
{
    use HasFactory;

    protected $fillable = [
        'name'
    ];
}
