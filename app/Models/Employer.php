<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property int id
 * @property string fname
 * @property string sname
 * @property string pname
 * @property Carbon created_at
 * @property Carbon updated_at
 *
 * Class Employer
 * @package App\Models
 */
class Employer extends Model
{
    use HasFactory;

    protected $fillable = [
        'fname',
        'sname',
        'pname',
    ];

    public function department(): HasMany
    {
        return $this->hasMany(Department::class);
    }
}
