<?php

namespace App\Http\Requests\Employer;

use Illuminate\Foundation\Http\FormRequest;

class EmployerGetRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'limit' => [
                'int',
                'min:1',
            ],
            'page' => [
                'int',
                'min:1',
            ],
            'per_page' => [
                'int',
                'min:1',
            ]
        ];
    }
}
