<?php

namespace App\Http\Requests\Employer;

use App\Dto\EmployerDto;
use Illuminate\Foundation\Http\FormRequest;

abstract class EmployerCommonRequest extends FormRequest
{
    public function rules():array
    {
        return [
            'fname' => [
                'required',
                'string',
                'max:255',
                'min:3',
            ],
            'sname' => [
                'required',
                'string',
                'max:255',
                'min:3',
            ],
            'pname' => [
                'required',
                'string',
                'max:255',
                'min:3',
            ]
        ];
    }

    public function getDto(): EmployerDto
    {
        return new EmployerDto(...$this->all());
    }
}
