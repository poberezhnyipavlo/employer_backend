<?php

namespace App\Http\Resources\Employer;

use App\Models\Employer;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class EmployerResource
 * @mixin Employer
 * @package App\Http\Resources\Employer
 */
class EmployerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'fname' => $this->fname,
            'sname' => $this->sname,
            'pname' => $this->pname,
        ];
    }
}
