<?php

namespace App\Http\Controllers;

use App\Http\Requests\Employer\EmployerGetRequest;
use App\Http\Requests\Employer\EmployerSearchRequest;
use App\Http\Requests\Employer\EmployerCommonRequest;
use App\Http\Requests\Employer\EmployerUpdateRequest;
use App\Http\Resources\Employer\EmployerResource;
use App\Http\Resources\Employer\EmployersCollection;
use App\Models\Employer;
use App\Services\EmployerService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class EmployerController extends Controller
{
    private EmployerService $employerService;

    public function __construct(EmployerService $employerService)
    {
        $this->employerService = $employerService;
    }

    public function index(EmployerGetRequest $request): JsonResponse
    {
        return response()->json(new EmployersCollection(
            $this->employerService->getAll($request->all())
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param EmployerCommonRequest $request
     * @return JsonResponse
     */
    public function store(EmployerCommonRequest $request): JsonResponse
    {
        $this->employerService->create($request->getDto());

        return response()->json();
    }

    /**
     * Display the specified resource.
     *
     * @param Employer $employer
     * @return JsonResponse
     */
    public function show(Employer $employer): JsonResponse
    {
        return response()->json(
            new EmployerResource($employer)
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param EmployerUpdateRequest $request
     * @param Employer $employer
     * @return Response
     */
    public function update(EmployerUpdateRequest $request, Employer $employer)
    {
        $this->employerService->update($request->getDto(), $employer);

        return response()->json();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Employer $employer
     * @return JsonResponse
     */
    public function destroy(Employer $employer): JsonResponse
    {
        if ($this->employerService->delete($employer)) {
            return response()->json();
        }
    }

    public function search(EmployerSearchRequest $request): JsonResponse
    {
        return response()->json(
            EmployerResource::collection(
                $this->employerService->search($request->get('search'))
            )
        );
    }
}
