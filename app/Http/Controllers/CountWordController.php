<?php

namespace App\Http\Controllers;

use App\Http\Resources\Word\WordResource;
use App\Services\WordCountInStringService;
use Illuminate\Http\Resources\Json\JsonResource;

class CountWordController extends Controller
{
    private WordCountInStringService $countWordService;

    public function __construct(WordCountInStringService $countWordService)
    {
        $this->countWordService = $countWordService;
    }

    public function __invoke(): array
    {
        return $this->countWordService->countWord();
    }
}
