<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\AuthRequest;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param AuthRequest $request
     * @return JsonResponse
     */
    public function __invoke(AuthRequest $request): JsonResponse
    {
        Auth::attempt($request->validated());

        /** @var User $user */
        $user = \auth()->user();

        if ($user) {
            $token = $user->createToken('bearer');
            return response()->json(['token' => $token->plainTextToken], 200);
        }

        return \response()->json(['error' => 'Ви ввели не правильний емейл або пароль'], 401);
    }
}
