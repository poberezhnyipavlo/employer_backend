<?php

namespace App\Dto;

final class EmployerDto
{
    private string $fname;
    private string $sname;
    private string $pname;

    public function getFname(): string
    {
        return $this->fname;
    }

    public function setFname(string $fname): void
    {
        $this->fname = $fname;
    }

    public function getSname(): string
    {
        return $this->sname;
    }

    public function setSname(string $sname): void
    {
        $this->sname = $sname;
    }

    public function getPname(): string
    {
        return $this->pname;
    }

    public function setPname(string $pname): void
    {
        $this->pname = $pname;
    }

    public function __construct(string $fname, string $sname, string $pname)
    {
        $this->fname = $fname;
        $this->sname = $sname;
        $this->pname = $pname;
    }
}
