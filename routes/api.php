<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CountWordController;
use App\Http\Controllers\EmployerController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', AuthController::class);

Route::apiResource('employers', EmployerController::class);
Route::post('employers/search', [EmployerController::class, 'search']);

Route::get('word', CountWordController::class);
